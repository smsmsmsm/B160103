#include<stdio.h>
#include<string.h>
#include<conio.h>
#include<stdlib.h>
int Number=20;                                     /*设定全局变量记录初始用户数量，方便修改*/
struct User
{
    char usernumber[20];                           /*存单编号*/
    char name[20];                                 /*用户姓名*/
    char IDnumber[20];                             /*身份证号*/
    char address[30];                              /*住址*/
    char phonenumber[20];                          /*联系电话*/
    float put_in,interest,remain;                  /*存入金额，利率，余额*/
    int cunqi;                                     /*存期*/
    char code[6];                                  /*用户账户密码*/
};
typedef struct User user;
int readFile(user st[]);                           //读入文件中已有的用户信息
void workercheck();                                //工作人员进入系统，修改系统密码
void Userfunction(user st[]);                      //用户功能选择函数
void readUser(user st[]);                          //录入新用户信息
int deleteUser(user st[]);                         //删除用户信息
void changeaddress(user st[],int i);               //修改地址
void changephonenum(user st[],int i);              //修改电话
void changecode(user st[],int i);                  //修改密码
int inquiry(user st[]);                            //查询用户信息
void Output(int i,user st[]);                      //显示指定用户所有信息
void checkcode(user st[],int i);                   //核查密码是否正确
/*int insertUser(user st[],int n,user s);*/        //待重写

int main()                                         //执行程序主函数
{
    user UI[10000];                                //设定一个足够大容量的库存放用户信息
    int n;                                         //用来存放用户选项
    system("color f0");                            //将运行背景改为白色
    n=readFile(UI);                                //读入已有用户信息存放在UI[]中
    for(;;)                                        //重复执行，直至用户输入3退出
    {
        printf("\tWhich kind person of you?\n\t(1.Worker  2.User  3.Out)\n");
        printf("\t");
        scanf("%d",&n);                            //读入用户选项
        switch(n)
        {
            case 1: workercheck();                 //为1执行工作人员相应功能
                    break;
            case 2: Userfunction(UI);              //为2执行用户相应功能
                    break;
            case 3: break;
            default: printf("Wrong input.\n");     //为其他选项提示错误输入
                     break;
         }
         if(n==3)                                  //为3退出程序
         {
             printf("\tThank you for your use.\n");
             break;
         }
         printf("\n");                                      //下一次执行与前一次隔一行
     }
    return 0;
}
int readFile(user st[ ])                                   //将文件中的内容读出置于结构体数组stu中
{
   	FILE *fp;
	int i=0;
	if((fp=fopen("F:\\yonghu4.txt", "a+")) == NULL)         //以读的方式打开指定文件
	{
	    printf("file does not exist,create it first:\n");  //如果打开失败输出提示信息
	    return 0;
	}
   	while(!feof(fp))                                       //文件未结束时循环
	{
	    fscanf (fp,"%s%s%s%s%s%f%d%s",st[i].usernumber,st[i].name,st[i].IDnumber,st[i].address,st[i].phonenumber,&st[i].remain,&st[i].cunqi,st[i].code);
		switch(st[i].cunqi)
		{
        case 3:
            st[i].interest=0.6;
            break;
        case 6:
            st[i].interest=1.5;
            break;
        case 9:
            st[i].interest=1.8;
            break;
        case 12:
            st[i].interest=2.15;
            break;
        case 18:
            st[i].interest=2.75;
            break;
        case 24:
            st[i].interest=3.5;
            break;
		}                                                   //依据存期读入利率
		i++;
     }
	fclose(fp);                                             //关闭文件
	return i;                                               //返回记录条数
}
void workercheck()
{
    char str1[20],str2[20]="86683650",str3[20],str4[20],str5[20]="06060526";    //定义两个字符串数组，str2存放已知密码，str1存放输入密码
    int i=0,k,flag1=0,flag2=0,flag3=0,n;
    while (1)                                                                   //循环
    {
        printf("\tPlease input the system's code.\n");
        printf("\t");
        scanf("%s",str1);                                                       //输入进入系统密码保存在str1中
        k=strcmp(str1,str2);                                                    //比较，输入密码和已知密码相等，k=0
        ++flag1;                                                                //计数器加一，记录输入次数
        if(!k)
        {
            printf("\tWelcome to the system.\n");                               //正确则进入系统
            break;
        }
        else if(k&&flag1<3)
            printf("\tWrong code,input correctly!\n");                          //错误则重新输入
        if(flag1==3)                                                            //当输入达到三次，跳出循环，结束程序
        {
            printf("\tSorry,you have no right to the system.\n");
            break;
        }
    }
    printf("\t(Reset system's code please input 1.)");                          //如果工作人员需要修改系统密码输入1
    printf("\n\t");
    scanf("%d",&n);
    for(;;)
    {
         if(n==1)                                                               //输入1则开始修改系统密码
        {
            printf("\tPlease input the system's changecode.\n");
            printf("\t");
            scanf("%s",str1);                                                   //修改系统密码需要输入最高密码
            k=strcmp(str1,str5);                                                //比较，输入密码和已知密码相等，k=0
            ++flag2;                                                            //计数器加一
            if(!k)                                                              //正确则开始修改密码
            {
               system("cls");
               for(;;)
              {
                   printf("\tInput new code: ");
                   scanf("%s",str3);                                            //第一次输入新的系统密码
                   printf("\n");
                   system("cls");
                   printf("\tInput new code again: ");
                   scanf("%s",str4);                                            //第二次输入新的系统密码
                   if(strcmp(str3,str4))                                        //比较两次是否一致
                   {
                       printf("\tTwo strings of code are different.\n\tPlease input correctly.\n");    //不一致重新输入
                       continue;
                   }
                   else
                   {
                       printf("\tFinished.\n");                                 //一致则完成修改
                       flag3=1;
                       break;
                   }
               }
             }
             else if(k&&flag2<3)                                               //输入系统密码错误则重新输入
            {
                printf("\tWrong code,input correctly.\n");
            }
            if(flag2==3)                                                      //当输入达到三次，无权修改密码，跳出循环
            {
                printf("\tSorry,you have no right to reset the system's code.\n");
            }
            if(flag3==1)                                                      //修改系统密码成功退出功能的标志
            {
                break;
            }
        }
        else
        {
            printf("\tWrong input.\n\t(Reset system's code please input 1.)\n");  //输入除1以外其他功能键则提示错误输入，重新输入
            printf("\t");
            scanf("%d",&n);
        }
    }

}
void Userfunction(user st[])
{
    int n,a,b;
    printf("\tOur functions:\n");
    printf("\t1.Input a new user'information.\n\t2.Delete your user's information.\n");
    printf("\t3.Change your information.\n\t4.Inquire your information.\n");                 //用户功能选择界面
    printf("\t");
    scanf("%d",&n);                                                                          //n为供用户选择功能几的
    switch(n)
    {
         case 1: readUser(st);                                                               //功能1：录入新用户信息
                 break;

         case 2: deleteUser(st);                                                             //功能2：注销用户信息
                 break;

         case 3: b=inquiry(st);
                 checkcode(st,b);
                 printf("\tWhat do you want to change?\n");                                 //功能3：修改信息
                 printf("\t1.Address.\t2.Phonenumber.\t3.Code.\n");
                 printf("\t");
                 scanf("%d",&n);
                 if(n==1)                                                                   //1为修改地址
                 {
                        changeaddress(st,b);
                 }else if(n==2)                                                             //2为修改联系电话
                 {
                        changephonenum(st,b);
                 }else if(n==3)                                                             //3为修改密码
                 {
                        changecode(st,b);
                 }else
                 {
                        printf("\tWrong input.\n");
                 }
                 break;
        case 4:  a=inquiry(st);
                 checkcode(st,a);                                                             //功能4：查询信息，a表示第几个用户
                 Output(a,st);                                                              //输出这个用户信息
                 break;
        default: printf("\twrong input.\n");                                                //其他功能键为错误输入
                 break;
    }
}
void readUser(user st[])                                                                   //录入新用户信息函数
 {
	     int i,n,a;
	     char code1[8],code2[8];                                                           //分两个数组记录两次密码
	     FILE *fp;
         i=Number+1;
         fp=fopen("F:\\yonghu4.txt","a+");
         if(fp==0)
         {
             printf("file error");
             exit(1);
         }
		 printf("\t输入用户信息\n");
		 printf("\tusernumber:   ");
         scanf("%s",st[i].usernumber);
		 printf("\n");
		 printf("\tname:  ");
		 scanf("%s",st[i].name);
		 printf("\n");
		 printf("\tIDnumber:  ");
         scanf("%s",st[i].IDnumber);
		 printf("\n");
		 printf("\taddress:    ");
		 scanf("%s",st[i].address);
		 printf("\n");
		 printf("\tphonenumber:   ");
         scanf("%s",st[i].phonenumber);
		 printf("\n");
		 printf("\tput_in:    ");
		 scanf("%f",&st[i].remain);
		 printf("\n");
		 printf("\tcunqi( 3 / 6 / 9 / 12 / 24 months):  ");
		 for(;;)
         {
             scanf("%d",&st[i].cunqi);
             printf("\n");
             switch(st[i].cunqi)
		     {
		        case 3: printf("\tYour interest is 0.60%%.\n");
		                st[i].interest=0.60;
		                break;
		        case 6: printf("\tYour interest is 1.50%%.\n");
                        st[i].interest=1.50;
		                break;
		        case 9: printf("\tYour interest is 1.80%%.\n");
		                st[i].interest=1.80;
		                break;
		        case 12: printf("\tYour interest is 2.15%%.\n");
		                 st[i].interest=2.15;
		                 break;
		        case 24: printf("\tYour interest is 3.50%%.\n");
		                 st[i].interest=3.50;
		                 break;
		        default: printf("\tWrong cunqi.Input again.( 3 / 6 / 9 / 12 / 24 months)\n\t");
		                 break;
		     }
		     if(st[i].cunqi==3||st[i].cunqi==6||st[i].cunqi==9||st[i].cunqi==12||st[i].cunqi==24)
             {
                 break;
             }
         }

         for(;;)
         {
             printf("\tPlease input your  code: ");
             scanf("%s",code1);
             printf("\n");
             system("cls");
             printf("\tPlease input your  code again: ");
             scanf("%s",code2);
             printf("\n");
             if(strcmp(code1,code2))
              {
                  printf("\tTwo strings of code are different.\n\tPlease input correctly.\n");
                  continue;
              }
              else
              {
                  strcpy(st[i].code,code1);
                  fputs(code1,fp);
                  printf("\tYour information has been recorded.\n");
                break;
              }

          }
	     Number++;
	     fclose(fp);
}
int deleteUser(user st[])                     /*删除用户信息函数*/
{
     int i,j,rightperson;
     char un[12];
     for(;;)
     {
         printf("\tPlease input your usernumber.\n");
         printf("\t");
         scanf("%s",un);
         for(i=0;i<10000;i++)
        {

             if(strcmp(un,st[i].usernumber))
             {
                 continue;
             }
             else
             {
                 rightperson=i;
                 break;
             }
         }
         if(i>=10000)
        {
            printf("\tWrong usernumber.\n");
        }
        else
        {
            break;
        }
      }
     checkcode(st,rightperson);
	 for (j=i;j<Number-1;j++)
    {
		 strcmp(st[j].usernumber,st[j+1].usernumber);
		 strcmp(st[j].name,st[j+1].name);
		 strcmp(st[j].IDnumber,st[j+1].IDnumber);
		 strcmp(st[j].address,st[j+1].address);
		 strcmp(st[j].phonenumber,st[j+1].phonenumber);
		 strcmp(st[j].code,st[j+1].code);
		 st[j].put_in=st[j+1].put_in;
		 st[j].remain=st[j+1].remain;
		 st[j].interest=st[j+1].interest;
		 st[j].cunqi=st[j+1].cunqi;
    }
     Number--;
	printf("\tYour information has been deleted.\n");
}
void changeaddress(user st[],int i)                   //修改地址函数
{
    char newaddress[20];                              //存放新地址
    int n;
    for(;;)
    {
        printf("\tPlease input your new address:");
        scanf("%s",newaddress);                           //读入新地址
        strcpy(st[i].address,newaddress);                 //用新地址替换原地址
        printf("\n");
        printf("\tYour new address is %s.\n\tAnywhere else need to change?(1--yes,0--no)\n",st[i].address);   //核实新地址，还需改动输入1，无误输入0
        printf("\t");
        scanf("%d",&n);
        for(;;)
        {
            if(n==1)                                          //输入为1重新改地址
           {
               break;
           }
           else
           {
               if(n==0)                                     //输入为0退出
               {
                   printf("\tYour address has been changed successfully.\n");
                   break;
               }
               else                                             //输入其他则重新输入
               {
                   printf("\tWrong input.\n");
                   printf("\tYour new address is %s.\n\tAnywhere else need to change?(1--yes,0--no)\n",st[i].address);
                   printf("\t");
                   scanf("%d",&n);
               }
           }
           if(n==0)                                            //跳出循环
           {
               break;
           }
        }
        if(n==1)                                              //排除错误输入后1为再次修改，0为结束功能
        {
            continue;
        }
        else
        {
            break;
        }
    }
}
void changephonenum(user st[],int i)                   //修改联系电话函数
{
    char newphonenum[20];
    int n;
    for(;;)
    {
        printf("\tPlease input your new phonenumber:");           //输入新联系电话
        scanf("%s",newphonenum);
        strcpy(st[i].phonenumber,newphonenum);                    //用新电话替换原电话
        printf("\n");
        printf("\tYour new phonenumber is %s.\n\tAnywhere else need to change?(1--yes,0--no)\n",st[i].phonenumber);   //显示新电话，给用户核查
        printf("\t");
        scanf("%d",&n);
        for(;;)
        {
            if(n==1)                                          //输入为1重新改电话号码
           {
               break;
           }
           else
           {
               if(n==0)                                     //输入为0退出
               {
                   printf("\tYour phonenumber has been changed successfully.\n");
                   break;
               }
               else                                             //输入其他则重新输入
               {
                   printf("\tWrong input.\n");
                   printf("\tYour new phonenumber is %s.\n\tAnywhere else need to change?(1--yes,0--no)\n",st[i].phonenumber);
                   printf("\t");
                   scanf("%d",&n);
               }
           }
           if(n==0)                                            //跳出循环
           {
               break;
           }
        }
        if(n==1)                                              //排除错误输入后1为再次修改，0为结束功能
        {
            continue;
        }
        else
        {
            break;
        }
    }
}
void changecode(user st[],int i)                     //修改密码函数
{
    char newcode1[6],newcode2[6];                    //提供两个数组存放两次输入的密码
    int n,a,m;
    checkcode(st,i);
    for(;;)
    {
        system("cls");
        printf("\tPlease input your new code: ");    //第一次读入密码
        scanf("%s",newcode1);
        system("cls");
        printf("\tPlease input your new code again: ");       //第二次读入密码
        scanf("%s",newcode2);
        printf("\n");
        if(strcmp(newcode1,newcode2))                    //比较两次密码是否一致
        {
            printf("\tTwo strings of code are different.\n\tPlease input correctly.\n");   //不一致重新输入
            continue;
        }
        else
        {
            strcpy(st[i].code,newcode1);                      //一致用新密码替换原密码
            printf("\tYour code has been changed successfully.\n");
            break;
        }
    }
}
void checkcode(user st[],int i)                     //核查用户密码函数
{
    char mima[7];
    int n=5;                                          //n为如果输错，用来记录剩余输入密码的机会次数
    printf("\tPlease input your code.\n\tYou have %d chance(s).\n",n);
    n--;
    printf("\tYour code is:  ");
    for(;;)
    {
        scanf("%s",mima);
        printf("\n");                           //读入密码
        if(strcmp(mima,st[i].code))                 //比较输入的密码和用户信息是否一致
        {
            printf("\tWrong code!\n\tYou have %d chance(s).\n\t",n);        //密码错误的话再次输入，并提示还有几次机会
            n--;
            if(n==0)
            {
                printf("\tYour have no right to enter.\n");            //五次机会用完，无权进入
                break;
            }
        }
        else
        {
            printf("\tRight code!\n");
            break;
        }
    }
}
int inquiry(user st[])                              //查询用户函数
{
    int i,j,rightperson;                            //rightperson记录该用户在结构体数组中的位置
    char un[13];                                    //用以存放输入的用户编号
    for(;;)
    {
         printf("\tPlease input your usernumber.\n");
         printf("\t");
         scanf("%s",un);                             //读入用户编号
         for(i=0;i<10000;i++)                        //在UI数组中查询与之匹配的用户编号
        {

             if(strcmp(un,st[i].usernumber))         //如果不匹配则比较下一个
             {
                 continue;
             }
             else                                    //如果匹配则用rightperson记录该用户位置
             {
                 rightperson=i;
                 break;
             }
         }
         if(i>=10000)                                //如果查询不到说明输入用户编号有误，再次输入
        {
            printf("\tWrong usernumber.\n");
        }
        else                                          //查询到匹配的则跳出循环
        {
            break;
        }

    }
    return rightperson;                              //函数返回rightperson，便于告知该用户信息位置
}
void Output(int i,user st[])                        //输出用户信息函数
{
    printf("\tusernumber:");                         //输出用户编号
    puts(st[i].usernumber);
    printf("\tname:");                               //输出姓名
    puts(st[i].name);
    printf("\tIDnumber:");                           //输出身份证号
    puts(st[i].IDnumber);
    printf("\taddress:");                            //输出住址
    puts(st[i].address);
    printf("\tphonenumber:");                        //输出联系电话
    puts(st[i].phonenumber);
    printf("\tremain:%.2f yuan\n\tcunqi:%d month(s)\n",st[i].remain,st[i].cunqi);     //输出余额，存期
    printf("\tinterest:%.2f %%\n",st[i].interest);                       //输出利率
}


/* int insertStu(user st[],int n,user s)
 {
	     int i;
		 for(i=0;i<n;i++)
		 {
			 if (user[i]s)
			 {
				 printf("重复\n");
				 return 0;
			 }
		 }
		 for(i=n-1;i>=0;i--)
		 {
			 if(!larger(user[i],s,i)
			 break;
			 user[i+1]=user[i];
		 }
		 user[i+1]=s;
		 n++;
		 return n;
 }*/
















#include<stdio.h>
#include<string.h>
int Number=20;                                     /*设定全局变量记录初始用户数量，方便修改*/
struct User
{
    char usernumber[12];                           /*存单编号*/
    char name[20];                                 /*用户姓名*/
    char IDnumber[20];                             /*身份证号*/
    char address[20];                              /*住址*/
    char phonenumber[20];                          /*联系电话*/
    float put_in,interest,remain;                  /*存入金额，利率，余额*/
    int cunqi;                                     /*存期*/
    char code[6];                                  /*用户账户密码*/
};
typedef struct User user;
#define sizeUser sizeof(user)
void readUser(user st[]);
/*int insertUser(user st[],int n,user s);*/        /*待重写*/
int deleteUser(user st[]);
void Output(int i,user st[]);                      /*显示指定用户所有信息*/
int inquiry(user st[]);                            /*查询用户信息*/
void checkcode(user st[],int i);                   /*核查密码是否正确*/
void changeaddress(user st[],int i);               /*修改地址*/
void changephonenum(user st[],int i);              /*修改电话*/
void changecode(user st[],int i);                  /*修改密码*/

int main()                                         /*执行程序主函数*/
{
    user UI[10000];                                /*设定一个足够大容量的库存放用户信息*/
    int n,a;
    printf("\tOur functions:\n");
    printf("1.Input a new user'information.\n2.Delete your user's information.\n");
    printf("3.Change your information.\n4.Inquire your information.");
    scanf("%d",&n);                                /*n为供用户选择功能几的*/
    switch(n)
    {
         case 1: readUser(UI);break;               /*功能1：录入新用户信息*/

         case 2: deleteUser(UI);break;             /*功能2：注销用户信息*/

         case 3: printf("What do you want to change?\n");             /*功能3：修改信息*/
                 printf("1.Address.\t2.Phonenumber.\t3.Code.");
                 scanf("%d",&n);
                 if(n==1)                                             /*1为修改地址*/
                 {

                 }else if(n==2)                                       /*2为修改联系电话*/
                 {

                 }else if(n==3)                                       /*3为修改密码*/
                 {

                 }else
                 {

                 }
                 break;
        case 4:  a=inquiry(UI);                     /*功能4：查询信息，a表示第几个用户*/
                 Output(a,UI);                                        /*输出这个用户信息*/
                 break;
        default: printf("wrong input.\n");
                 break;
    }
    return 0;
}
void Output(int i,user st[])                        /*输出用户信息函数*/
{
    printf("usernumber:");
    puts(st[i].usernumber);
    printf("name:");
    puts(st[i].name);
    printf("IDnumber:");
    puts(st[i].IDnumber);
    printf("address:");
    puts(st[i].address);
    printf("phonenumber:");
    puts(st[i].phonenumber);
    printf("remain:%.2f yuan\n interest:%.2f\%\n cunqi:%d month(s)\n",st[i].remain,st[i].interest,st[i].cunqi);
}
int inquiry(user st[])                              /*查询用户函数*/
{
    int i,rightperson;
    char un[12];
    printf("Please input your usernumber.\n");
    for(;;)
    {
        for(i=0;i<12;i++)
        {
             scanf("%c",&un[i]);
             if(un[i]=='\n')
            {
                printf("The number of un is illegal.");
                break;
            }
        }
        if(i==12&&un[11]!='\n')
        {
            break;
        }
    }
    for(i=0;i<Number;i++)
    {

        if(strcmp(un,st[i].usernumber))
        {
            continue;
        }
        else
        {
            rightperson=i;
        }
    }
    return rightperson;
}
void checkcode(user st[],int i)                     /*核查用户密码函数*/
{
    char mima[6];
    int n=5;
    printf("Please input your code.\nYou have %d chance(s).\n",n);
    n--;
    for(;;)
    {
        gets(mima);
        if(strcmp(mima,st[i].code))
        {
            printf("Wrong code!\nYou have %d chance(s).\n",n);
            n--;
            if(n==0)
            {
                printf("Please go away.\n");
                break;
            }
            continue;
        }
        else
        {
            printf("Right!\n");
            break;
        }
    }
}
void changeaddress(user st[],int i)                   /*修改地址函数*/
{
    char newaddress[20];
    int n;
    for(;;)
    {
        printf("Please input your new address:");
        gets(newaddress);
        strcpy(st[i].address,newaddress);
        printf("Your new address is %s.\nAnywhere else need to change?(1--yes,0--no)",st[i].address);
        scanf("%d",&n);
        if(n==1)
        {
            continue;
        }
        else
        {
            printf("Finished.\n");
            break;
        }
    }
}
void changephonenum(user st[],int i)                   /*修改联系电话函数*/
{
    char newphonenum[20];
    int n;
    for(;;)
    {
        printf("Please input your new phonenumber:");
        gets(newphonenum);
        strcpy(st[i].phonenumber,newphonenum);
        printf("Your new phonenumber is %s.\nAnywhere else need to change?(1--yes,0--no)",st[i].phonenumber);
        scanf("%d",&n);
        if(n==1)
        {
            continue;
        }
        else
        {
            printf("Finished.\n");
            break;
        }
    }
}
void changecode(user st[],int i)                     /*修改密码函数*/
{
    char newcode1[6],newcode2[6];
    int n;
    for(;;)
    {
        printf("Please input your new code: ");
        gets(newcode1);
        printf("Please input your new code again: ");
        gets(newcode2);
        if(strcmp(newcode1,newcode2))
        {
            printf("Two strings of code are different.\nPlease input correctly.\n");
            continue;
        }
        else
        {
            strcpy(st[i].code,newcode1);
            printf("Finished.\n");
            break;
        }
    }
}
void readUser(user st[])                        /*录入新用户信息函数*/
 {
	     int i=Number+1;
		 printf("输入用户信息\n");
		 printf("usernumber:   ");
		 scanf("%s",st[i].usernumber);
		 printf("\n");
		 printf("name:  ");
		 scanf("%s",st[i].name);
		 printf("\n");
		 printf("IDnumber:  ");
		 scanf("%s",st[i].IDnumber);
		 printf("\n");
		 printf("address:    ");
		 scanf("%s",st[i].address);
		 printf("\n");
		 printf("phonenumber:   ");
		 scanf("%s",st[i].phonenumber);
		 printf("\n");
		 printf("put_in:    ");
		 scanf("%.2f",&st[i].put_in);
		 printf("\n");
		 printf("cunqi:  ");
		 scanf("%d",&st[i].cunqi);
		 printf("\n");
		 printf("code:   ");
		 scanf("%s",st[i].code);
	     Number++;
 }
/* int insertStu(user st[],int n,user s)
 {
	     int i;
		 for(i=0;i<n;i++)
		 {
			 if (user[i]s)
			 {
				 printf("重复\n");
				 return 0;
			 }
		 }
		 for(i=n-1;i>=0;i--)
		 {
			 if(!larger(user[i],s,i)
			 break;
			 user[i+1]=user[i];
		 }
		 user[i+1]=s;
		 n++;
		 return n;
 }*/
 int deleteUser(user st[])                     /*删除用户信息函数*/
{
     int i,j,rightperson;
     char un[12];
     printf("Please input your usernumber.\n");
     for(;;)
     {
        for(i=0;i<12;i++)
        {
             scanf("%c",&un[i]);
             if(un[i]=='\n')
            {
                printf("The number of un is illegal.");
                break;
            }
        }
        if(i==12&&un[11]!='\n')
        {
            break;
        }
     }
     for(i=0;i<Number;i++)
     {

        if(strcmp(un,st[i].usernumber))
        {
            continue;
        }
        else
        {
            rightperson=i;
        }
     }
	 for (j=i;j<Number-1;j++)
		st[j]=st[j+1];
     Number--;
	printf("Finished.");
}













